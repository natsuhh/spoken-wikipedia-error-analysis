#! /bin/bash

# installs into ./sequitur
# creates temporary files in current working directory

# checks if all dependencies for sequitur are installed
# downloads the installation file
# sets up a virtual environment
# installs sequitur and creates a SOURCEME and a apply_model.sh file
# call it like this: ./apply_model.sh <model_file> <wordlist_file>

echo "You also need SWIG installed and a c++ compiler (e.g. gcc)"

# dependencies
command -v swig >/dev/null 2>&1 || \
    { echo >&2 "swig is required but it's not installed.  Aborting."; exit 1; }
command -v python2.7 >/dev/null 2>&1 || \
    { echo >&2 "python2.7 is required but it's not installed.  Aborting."; exit 1; }
command -v pip >/dev/null 2>&1 || \
    { echo >&2 "pip is required but it's not installed.  Aborting."; exit 1; }
[ -f /usr/local/bin/virtualenvwrapper.sh ] || [ -f $HOME/.local/bin/virtualenvwrapper.sh ] || \
    { echo >&2 "virtualenvwrapper is required but it's not installed.  Aborting."; exit 1; }


sequitur_filename="g2p-r1668-r3.tar.gz"  # which version to install
if [ ! -f $sequitur_filename ]; then
    wget "http://www-i6.informatik.rwth-aachen.de/web/Software/$sequitur_filename"
    tar -xf $sequitur_filename # creates directory called g2p
fi

echo "loading virtualenvwrapper"
VIRTUALENVWRAPPER_PYTHON=$(which python3)
[ -f /usr/local/bin/virtualenvwrapper.sh ] && . /usr/local/bin/virtualenvwrapper.sh # ubuntu
[ -f $HOME/.local/bin/virtualenvwrapper.sh ] && . $HOME/.local/bin/virtualenvwrapper.sh # suse

echo "Creating virtualenv 'sequitur'"
mkvirtualenv -p $(which python2.7) sequitur
echo "Installing numpy"
pip install numpy

mkdir -p sequitur
cd g2p
python setup.py install --prefix ../sequitur || \
    { echo >&2 "setup failed.  Aborting."; exit 1; }
python setup.py install --prefix ../sequitur # second run required to copy sequitur_.py
cd -

cd sequitur/lib/python2.7/site-packages/
PYTHONPATH="$(pwd)"
export PYTHONPATH
cd -

cat >"sequitur/SOURCEME" <<EOF
PYTHONPATH=$PYTHONPATH
export PYTHONPATH
EOF

cat >"sequitur/apply_model.sh" <<EOF
#! /bin/bash
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=\$(readlink -f "\$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=\$(dirname "\$SCRIPT")
source \$SCRIPTPATH/SOURCEME

VIRTUALENVWRAPPER_PYTHON=\$(which python3)
[ -f /usr/local/bin/virtualenvwrapper.sh ] && . /usr/local/bin/virtualenvwrapper.sh # ubuntu
[ -f \$HOME/.local/bin/virtualenvwrapper.sh ] && . \$HOME/.local/bin/virtualenvwrapper.sh # suse

\$SCRIPTPATH/bin/g2p.py --model \$1 --apply \$2 --encoding=UTF-8
EOF
chmod +x sequitur/apply_model.sh

echo
if [ -x ./sequitur/bin/g2p.py ]; then
    rm -r g2p $sequitur_filename
    echo "Install successfull. To set your PYTHONPATH:"
    echo "source sequitur/SOURCEME"
    echo 
    echo "Try './sequitur/bin/g2p.py --help'"
else
    echo "Installation failed"
    exit 1
fi
