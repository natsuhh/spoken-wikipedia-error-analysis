#!/usr/bin/env python3
from collections import defaultdict
from collections import Counter
import enchant
import math
import numpy
import json
import random
import re
import matplotlib.pyplot as plt
import os
from scipy import stats
from sys import argv
from tinytag import TinyTag

# Filenames sind so semi-hardcoded, hier das base-directory ggf. ändern
WIKI_DIR = "/home/felix/nats/german-2/"
#RUN_DIR = "/home/felix/nats/run10/"
RUN_DIR = "/home/felix/nats/prosub/"
DEFAULT_SUMMARY = RUN_DIR + "summary.txt"
REGRESSION_FILE = "reg_de.json"
ARTICLES_FILE = "articles_de.json" # contains all the valid article names
LANG = 'en_US'


### parse summary

def read_summary_data(filename=DEFAULT_SUMMARY):
    with open(filename) as f:
        lines = [l for l in f.read().split("\n") if l]
    return lines

    
def get_aligned(lines):
    return [l for l in lines
            if l[0] in "0123456789"]


def parse_line(l):
    l = l.split("\t")
    l[0] = float(l[0][:-1])
    l[1] = float(l[1])
    l[2] = l[2][2:-1]
    return l


def sort_rows(rows):
    return sorted(rows, key=lambda l: l[0])


def get_rows(filename=DEFAULT_SUMMARY):
    """Bastelt alles zusammen, gibt alle alignierten Artikel geordnet als Listen zurück"""
    return sort_rows([parse_line(l) for l in
                       get_aligned(read_summary_data(filename))])


def all_articles():
    """reads all article names from the summary file"""
    if os.path.isfile(ARTICLES_FILE):
        with open(ARTICLES_FILE) as f:
            return json.load(f)
    else:
        return [r[2] for r in get_rows()]


def random_articles(n = None):
    """take n random articles for sampling"""
    arts = all_articles()
    random.shuffle(arts)
    return arts if not n else arts[:n]


def rows_to_str(rows):
    return "\n".join(["\t".join([str(x) for x in l])
                      for l in rows])


def plot_bins(rows):
    xs = [l[0] for l in rows]
    plt.hist(xs, 10)
    plt.show()

    
## errors

def get_errors(lines):
    errors = [l for l in lines if "ERROR" in l]
    return [e.split("\t")[2][2:-1] for e in errors]


### alignment data

def read_data(art_name, is_filename = False):
    if is_filename:
        fn = art_name
    else:
        fn = RUN_DIR + "gen/{}/steps/align.data.json".format(art_name)
    with open(fn) as f:
        data = json.loads(f.read())
    return data


## audio duration

def word_is_mapped(w):
    return 'start' in w and 'stop' in w


def word_is_silent(w):
    return w['normalized'] == ""


def mapped_parts(data):
    return [(w["start"], w["stop"]) for w in data["words"]
            if "start" in w]


def unmapped_parts(art_name):
    total = get_total_duration(art_name)
    mapped = mapped_parts(read_data(art_name))
    unmapped = []
    unmapped.append((0, mapped[0][0]))
    for i in range(len(mapped) - 1):
        unmapped.append((mapped[i][1], mapped[i+1][0]))
    unmapped.append((mapped[-1][1], total))
    return unmapped


def sum_tuples(ts):
    return sum([t[1] - t[0] for t in ts])


def plot_bin_parts(parts):
    lens = [p[1] - p[0] for p in parts]
    plt.hist(lens, 10)
    plt.show()


def get_total_duration(art_name):
    fn = WIKI_DIR + "{}/audio.ogg".format(art_name)
    tags = TinyTag.get(fn)
    return tags.duration * 1000 # ms


def add_total_duration(metadata):
    try:
        td = get_total_duration(metadata['art_name'])
    except Exception:
        td = None
    metadata['duration'] = td


def articles_with_broken_audio():
    r = []
    for a in all_articles():
        try:
            dur = get_total_duration(a)
        except Exception:
            r.append(a)
    return r


def amount_audio_files(art_name):
    fn = WIKI_DIR + art_name
    files = os.listdir(fn)
    x = len([f for f in files if f.startswith("audio") and f.endswith(".ogg")])
    if x == 1:
        return 1
    else:
        return x - 1


def percentage_audio_mapped(art_name):
    mapped = sum_tuples(mapped_parts(read_data(art_name)))
    try:
        all = get_total_duration(art_name)
    except FileNotFoundError:
        return -1
    return mapped / all


def get_audio_data(articles):
    res = []
    for a in articles:
        print(a)
        mapped = sum_tuples(mapped_parts(read_data(a)))
        try:
            all = get_total_duration(a)
        except FileNotFoundError:
            all = -1
        res.append([a, mapped, all, mapped / all])
    return res 


### regression

def regression(art_name, data = None):
    """data can be provided to avoid loading it from file"""
    if not data:
        data = read_data(art_name)
    x, y = list(zip(*mapped_points(data)))
    slope, intercept, r_value, p_value, std_err  = stats.linregress(x, y)
    return {'art_name': art_name,
            'slope': slope,
            'intercept': intercept,
            'r_value': r_value,
            'p_value': p_value,
            'std_err': std_err,
            'wordcount': len(data['words']),
            'amnt_mapped': amount_mapped(data['words'])}


# def add_amount_mapped(metadata):
#     x = amount_mapped(read_data(metadata['art_name'])['words'])
#     metadata.update({'amnt_mapped': x})
# 
# 
# def add_word_count(metadata):
#     metadata['wordcount'] = len(read_data(metadata['art_name'])['words'])
    

def get_all_reg_data():
    """performs regression for all articles"""
    res = [regression(a) for a in all_articles()]
    for r in res:
        try:
            add_reader(r)
        except:
            r['reader'] = None
    return res


def load_reg_data():
    """loads saved regression data for all articles from file"""
    with open(REGRESSION_FILE) as f:
        return json.loads(f.read())


def best_articles():
    """articles sorted based on the correlation coefficient"""
    reg = load_reg_data()
    return [a['art_name'] for a in sorted(reg, key = key_fn('r_value'), reverse = True)]


def hist_plot_slopes(reg_data):
    slopes = [r['slope'] for r in reg_data]
    plt.hist(slopes, range(0, 1000, 50))
    plt.show()


## alignment plot

def mapped_points(art_data):
    points = []
    for i, w in enumerate(art_data["words"]):
        if word_is_mapped(w):
            points.append((i, w["start"] + (w["stop"] - w["start"]) / 2))
    return points
    

SUMMARY_FORMAT_STR = """
article: {},
duration: {},
slope: {},
intercept: {},
r_value: {},
p_value: {},
std. err.: {}
""".strip()


def plot_align(art_name):
    audio_length = get_total_duration(art_name)
    data = read_data(art_name)
    r = regression(art_name, data)
    x, y = list(zip(*mapped_points(data)))
    x_max = len(data["words"])
    last_x = max(x)
    summary = SUMMARY_FORMAT_STR.format(art_name,
                                        secs_to_mins(audio_length /
                                                     1000), round(r['slope']),
                                        round(r['intercept']),
                                        round(r['r_value'], 5),
                                        round(r['p_value'], 5),
                                        round(r['std_err'], 5))
    plt.title(art_name)
    plt.xlabel("Words")
    plt.ylabel("msecs")
    plt.plot(x, y, "ro", [x_max], [audio_length], "bs", [0, last_x],
             [r['intercept'], r['slope']*last_x + r['intercept']],
             "y-") 
    plt.text(len(data["words"]) * 2/3, 100, summary)
    plt.show()


### lücken im mapping und deren Auslöser

def find_gaps(art_name):
    art_data = read_data(art_name)
    gaps = []
    prev = {'start': 0, 'stop': 0}
    for i, w in enumerate(art_data['words']):
        if w['normalized'] == "":
            continue # ignore punctuation
        prev_mapped = word_is_mapped(prev)
        curr_mapped = word_is_mapped(w)
        if prev_mapped and not curr_mapped: # gap starting
            gaps.append({'words': [w], 'start': prev['stop']})
        elif not prev_mapped and not curr_mapped: # gap continues
            gaps[-1]['words'].append(w)
        elif not prev_mapped and curr_mapped: # gap ended
            gaps[-1]['stop'] = w['start']
        prev = w
    if not 'stop' in gaps[-1]:
        gaps[-1]['stop'] = get_total_duration(art_name)
    return gaps


def filter_gap_size(gaps, min_size):
    return [g for g in gaps if len(g['words']) >= min_size]


def hist_plot_gap_sizes(gaps):
    lens = [len(g) for g in gaps]
    plt.hist(lens)
    plt.show()


def gap_words(gaps):
    return sum([g['words'] for g in gaps], [])


def gap_starters(gaps):
    """Gibt Fremdwörter zurück, die zu einer Alignment-Lücke geführt haben können"""
    checker = enchant.Dict(LANG)
    ws = sum([[w['normalized'] for w in g['words'][:3]]
              for g in gaps], [])
    return [w for w in ws if not checker.check(w)]


def original_text(words):
    return "".join([w['original'] for w in words])


def distil_gaps(gaps):
    return sorted([(g['stop'] - g['start'], original_text(g['words']))
                   for g in gaps],
                  key = lambda g: g[0], # sort by duration
                  reverse = True)


### mismatches / tokenization errors

def poorly_normalized(word):
    if len(word['normalized'].replace(' ', '')) > len(word['original']):
        if len(word['normalized'].split()) > 3:
            if not re.match(".*\d{1}.*", word['original']):
                return True
    return False
    

def art_poorly_normalized(art_name, data = None):
    if not data:
        data = read_data(art_name)
    return [w for w in data['words'] if poorly_normalized(w)]


def help_analyze(art_name):
    data = read_data('../prosub/gen/{}/steps/normalize.data.json'.format(art_name), is_filename=True)
    return art_poorly_normalized(art_name, data)


### audio mapped multiple times

def time_frames_and_count(art_data, min_count = 2):
    time_frames = defaultdict(int)
    words = defaultdict(list)
    for w in art_data['words']:
        if word_is_mapped(w):
            key = (w['start'], w['stop'])
            time_frames.update({key: time_frames[key] + 1})
            words[key].append(w['normalized'])
    x = []
    for tf in time_frames:
        c = time_frames[tf]
        if c >= min_count:
            x.append({'start': tf[0], 'stop': tf[1],
                      'count': c, 'texts': list(set(words[tf]))})
    x = sorted(x, key = key_fn('count'), reverse = True)
    return x


def total_duplicate_mappings(tfs):
    return sum(tf['count'] for tf in tfs)


def avg_dup_count(tfs):
    return total_duplicate_mappings(tfs) / len(tfs)


def all_time_frames():
    res = []
    for a in all_articles():
        res.append({'art_name': a, 'time_frames': time_frames_and_count(read_data(a))})
    return res


### sentences

def reconstr_sentences(art_data):
    return [art_data['words'][s['start_word']:s['end_word']]
            for s in art_data['sentences']]


def complete(words):
    return all(word_is_mapped(w) for w in words
               if not word_is_silent(w))


def missing_words(words):
    return [w for w in words if not word_is_mapped(w) and not word_is_silent(w)]


def word_counts(words):
    return Counter(w['normalized'].lower() for w in words)


def summary_missing_words(art_name):
    return word_counts(missing_words(read_data(art_name)['words']))


def amount_mapped(words):
    return 1 - len(missing_words(words)) / len(words)


def plot_completeness_vs_fit(reg_datas):
    xs, ys, colors, labels = [], [], [], []
    for r in reg_datas:
        xs.append(r['slope'])
        ys.append(r['amnt_mapped'])
        colors.append(r['r_value']**4)
        labels.append(r['art_name'])
    plt.scatter(xs, ys, c=colors, s=50)
    plt.xlabel("Slope")
    plt.ylabel("Amount mapped")
    for x, y, label in zip(xs, ys, labels):
        plt.annotate(label, xy=(x, y), size='xx-small')
    plt.show()


def plot_r_value_vs_std_error(reg_datas):
    xs, ys, labels = [], [], []
    for r in reg_datas:
        xs.append(r['r_value']**8)
        ys.append(r['amnt_mapped']**2)
        labels.append(r['art_name'])
    plt.scatter(xs, ys, c = xs)
    plt.xlabel("R Value")
    plt.ylabel("Amount Mapped")
    for x, y, label in zip(xs, ys, labels):
        plt.annotate(label, xy=(x, y), size='xx-small')
    plt.show()



    
### analysis based on speakers
    
def get_reader(art_name):
    with open(WIKI_DIR + art_name + "/info.json") as f:
        j = json.loads(f.read())
    return j['audio_file']['reader']


def add_reader(metadata):
    reader = get_reader(metadata['art_name'])
    metadata.update({'reader': reader})


### utility

def dump_to(obj, filename):
    with open(filename, 'w') as f:
        json.dump(obj, f, indent = 2)


def key_fn(i):
    return lambda x: x[i]


def secs_to_mins(secs):
    mins = math.floor(secs/60)
    secs = round(secs % 60, 1)
    return "{}:{}".format(mins, secs)


def for_all_articles(fn, key = 0):
    arts = all_articles()
    r = []
    for i, a in enumerate(arts):
        if i % 50 == 0:
            print("{}/{}".format(i, len(arts)))
        r.append((a, fn(a)))
    if key != 0:
        r = sorted(r, key = key_fn(abs(key)), reverse = (key < 0))
    return r


## Aufrufen mit Artikel-Name um einen Plot zu bekommen
if __name__ == "__main__":
    if len(argv) >= 2:
        art_name = argv[1]
        if art_name == "overview":
            plot_completeness_vs_fit(load_reg_data())
        elif art_name == "r_value":
            plot_r_value_vs_std_error(load_reg_data())
        else:
            print(json.dumps(time_frames_and_count(read_data(art_name)), indent = 2))
            plot_align(art_name)
    else:
        print("Missing article name argument")
