* NATS

** Notizen

- Wirklich nicht gematcht
  - Beispiele:
    - Spitzer Haaransatz

- Die, die in der Summary einen ERROR haben, erstmal garnicht betrachtet.

- Literatur wird am Ende nicht mitgemappt

- Stegen BA
  - multilinguale Akustikmodelle?
    - Ein großes Problem sind Fremdwörter in den Artikeln, die
      natürlich mit einem anderen Modell vorgelesen werden.


** TODOs

- /informatik2/nats/projects/spokenWikipedia/stegen-BA/code
- git@bitbucket.org:inpro/spokenwikipedia.git
  - geht nicht?
- https://bitbucket.org/natsuhh/prosub
  - muss ich noch hinzugefügt werden
- german-2 -> german
  - Problem: audio-duration Dateien nur in german-2
    - extra generiert von Timo
- Notizen in den Ordner legen
  - mir fehlen noch die Berechtigungen
- pdb ...

*** Mögliche Fehler
    - falsche Zuordnung von Landmarks, dazu muss es Landmarks doppelt
      geben.  Zu beobachten beim Lizenztext, der am Anfang und am Ende
      vorgelesen wird.  Noch relevantere Beispiele?
    - "Spitzer Haaransatz", Transkript richtig?  Hat komische
      Outliers, aber nur wenige Fremdwörter
    - Was für Audio-Transformationen werden vorgenommen?  VAD könnte
      scheitern bei starkem Hintergrundrauschen und hohen
      Frauenstimmen (siehe Fehler > Audio schlecht)


** Analysetechniken

*** lineare Regression
    - R-Value sollte 0.99 sein, ansonsten ist etwas faul, z.B.: Brainfuck
    - Slope, exp. vs actual: Audio entweder zu kurz oder zu lang
      - siehe hierzu auch Plots, slope sollte so im Bereich 400 bis
        600ms liegen
      - Deckt auf wenn mitten drin Audio fehlt

*** Matching Lücken finden
    - Nur interessant für Texte die eigentlich gut aligniert werden.
      Wenn die Audio-Qualität schlecht ist ist das egal.
    - Welche Wörter bringen den Algorithmus raus?
      - siehe: Fehler > andere Sprachen

*** lange, normalisierte Wörter
    - häufig Normalisierungsfehler (Kilometer, Seitenzahlen ...)


** Fehler

*** Musik mitten im Text
    - *lässt sich das erkennen? (kein Fehler)*
    - Willi Ostermann
    - Hansa Musik Produktion

*** Daten passen nicht
    - falscher Text zum Audio *Bug oder API-Problem?*
      - falsche Artikelversion
        - Yellow Submarine
        - Schwarzwald
      - falscher Artikel
        - Audio: Fluglotse, Text: A
        - Audio: Texas Chainsaw ..., Text: Alfred Wegener
        - Mohammed, Dresdener Zwinger
        - Hardware, Ostfriesland
          - Audio: Hardware; Text: Ostfriesland
          - oldid im Wiki-Template verlinkt auf den falschen Artikel
        - Fieberthermometer, Ostfriesland
        - Pangasius
    - mehrere Audio-Dateien
      - *Bug, align.json Datei unvollständig*
        - In der BA steht die Files wurden aneinandergereiht und dann
          verwendet.  Die align.json-Datei scheint nur Daten von der
          ersten Audio-Datei zu enthalten
      - Napoleon Bonaparte
      - Österreich in der Zeit des Nationalsozialismus
      - Deutschland
      - Deutsches Kaiserreich
    - Audio unvollständig *Datenproblem, autom. erkennbar?*
      - BDSM
      - Kulturapfel
      - Afghanischer Bürgerkrieg (nur Intro)
    - Audio schlecht *Datenproblem*
      - Alaskan Husky (schlechtes Mikro, kleines Mädchen?)
      - Tierschutz (Rauschen, Knacken)
      - Conradi-Affäre (schlechtes Mikro, schweizer Akzent)
      - Internetforum (schlechtes Mikro, führt aber nur zu Aussetzern
        in der Erkennung)
      - Radio Vatikan (z und sch verzerrt)

*** Normalisierung schlecht
    - *kein Fehler, aber ggf. zu verbessern*
      - mehrere Auswahlmöglichkeiten wenn die Phoneme wieder auf Text
        gematcht werden?
    - * 1. Oktober -> stern erste Oktober
      - gesprochen "geboren am ersten Oktober" (z.B.: Willi Ostermann)
    - "90." (Satzende) -> neunzigste
    - 1900 -> neun zehn null null
      - gesprochen "neun zehn hundert" (Willi Ostermann 3:36)
    - Papst Pius XI
      - gesprochen "Papst Pius der elfte" (Radio Vatikan 0:50)
    - LaTeX wird nicht vernünftig normalisiert, d =
      \frac{log(4)}{log(2)}
      - gesprochen "d gleich logarithmus von 4 durch logarithmus von 2
        (Sierpinski-Dreieck 8:14)
    - 1:200.000 "eins zwei hundert punkt null null null" (Schwarzwald)
    - "ha" sollte zu "Hektar" werden (Schwarzwald)
    - "sm" sollte zu "Seemeilen" werden (Motortorpedoboot)
    - alte Jahreszahlen (13XX) -> Tausend Drei Hundert null null

*** andere Sprachen
    - *akustisches Modell, kein Fehler*
    - Kölsche Mundart / wechselnder Sprecher im Text
      - Willi Ostermann 2:57
    - Französische Wörter (Engagement, Varieté)
      - Willi Osermann ~4:05
    - japanische Schriftzeichen (Kenroku-en 4:00)
    - "scrambling" (Quarterback)

*** Kilometer-Fehler u.Ä. *Bugs*
    - marry words anschauen
    - Kilometer
      - Isar
      - Schwarzwald
        - Hier mal verleichen von 
          - temp/aligned
          - steps/aligned.json
          - normalisierung total kaputt, wie kann "seine" auf
            "gerundeten" normalisiert werden?
    - Literatur-Abschnitt 
      - Links werden schlecht normalisiert (Argentinien-Krise, Ludwig
        Feuerbach) aber die sind nur in der Literatur, daher ggf nicht
        wichtig. 
      - S. <Seitenzahl>
        - Agnes Bernauer 11114 "S. 582"
        - Ludwig IV. (HRR) 13545
        - Ostfriesland 23963
      - <zahl> v. Chr.
        - Ostfriesland 3947
          - Der Chuncker geht auch total kaputt ...
        - Ostfriesland 4166
    -> Woher kommen die Fehler in der *align.json* Datei?
       *Normalisierung* und *Chunking* beeinflussen sich gegenseitig ...
