#! /bin/bash


install() {
    # assume that master_script.sh, install_sequitur.sh and install_sphinxtrain.sh are already downloaded
    # filegen_sphinxtrain.sh
    # or hardcode paths for wget
    bash master_script.sh -D -P -G -E
    bash install_sequitur.sh # installs to sequitur
    mkdir sphinxtrain
    bash install_sphinxtrain.sh --install --path sphinxtrain --clean
}


prepare() {
    if [ ! -e "articles" ]; then
        echo "Downloading articles"
        bash master_script.sh -l $language -I -P -G -E # download data, creates a directory called 'articles'
    fi
    echo "Preparing audio"
    bash master_script.sh -l $language -I -D -G -E # prepare data
    echo "Starting aligning for iteration00"
    mkdir -p iterations/iteration00/alignment_results
    bash master_script.sh -I -D -P -o iterations/iteration00/alignment_results \
         -l $language -m "$model" -s "$ser" -d "$dic"
}

language=german  #german|english
ffmpeg=ffmpeg
G2P_model=""
G2P_model_CAPS=false #true|false
model=""
ser=""
dic=""


iteration() {
    # generate snippets for training
    # generate other stuff for training
    # train new model (sphinxtrain)
    # align stuff
    # hmm. for now just assume the SnippetExtractor.jar is there --> TODO
    itname=$1 # is iteration name/number "iteration03" or something like that
    gendir=$2
    model=$3
    snippetsdir=iterations/$itname/wav
    ls -1 "$gendir" | \
        while read dir; do
            if [ ! -e "$snippetsdir/$dir" ]; then
                echo "Generating snippets for $dir"
                mkdir -p "$snippetsdir/$dir"
                timeout 30m java -jar SnippetExtractor.jar -t -- "$gendir/$dir/steps/align.data.json" \
                        "articles/$dir/audio.wav" "$snippetsdir/$dir" $ffmpeg
                rt=$?
                if [ $rt -eq 124 ]; then
                    echo "Generating snippets for $dir - timeout"
                else
                    echo "Generating snippets for $dir - done"
                fi
            else
                echo "Generating snippets for $dir - skipped"
            fi
        done
    caps=""
    [ G2P_model_CAPS = "true" ] && caps="--caps"
    ./filegen_sphinxtrain.py gen --prefix iterations/$itname --no_test $caps \
                             sequitur/apply_model.sh $G2P_model $itname
    . sphinxtrain/SOURCEME # source env vars
    cd iterations/$itname
    sphinxtrain -t $itname setup
    cd -
    ./filegen_sphinxtrain.py cfg iterations/$itname/etc/sphinx_train.cfg
    cd iterations/$itname
    sphinxtrain --stages comp_feat,verify,lda_train,mllt_train,ci_hmm,cd_hmm_untied,buildtrees,prunetree,cd_hmm_tied run
    cd -
    bash master_script.sh -I -D -P -o iterations/$itname/alignment_results \
         -l $language -m "$model" -s "$ser" -d "$dic"
}


bootstrap() {
    prepare
    echo "Preparation and iteration00 finished"
    n=$1
    i=0
    while [[ $i -lt $n ]]; do
        prev_itname=$(printf "iteration%02d" $i)
        let i=i+1
        itname=$(printf "iteration%02d" $i)
        echo "Starting iteration $itname"
        iteration $itname iterations/$prev_itname/alignment_results iterations/$prev_itname
    done
}


# all sphinxtrain stages
# comp_feat,verify,g2p_train,lda_train,mllt_train,vector_quantize,falign_ci_hmm,force_align,vtln_align,ci_hmm,cd_hmm_untied,buildtrees,prunetree,cd_hmm_tied,lattice_generation,lattice_pruning,lattice_conversion,mmie_train,deleted_interpolation,decode


printHelp() {
    echo "Options:"
    echo "-i, --install                    will install all the required components and exit"
    echo "-l, --language <english|german>  which language to use. default: german"
    echo "-f, --ffmpeg <path>              path to an ffmpeg binary, if its not in PATH"
    echo "-g, --g2p <path>                 path to a g2p model required by sequitur"
    echo "-c, --caps                       flag if the model is in all caps"
    echo "-m, --model <path>               path to the initial model to use "
    echo "-s, --ser <path>                 path to the ser file required by the aligner"
    echo "-d, --dic <path>                 path to the dic file required by the aligner"
    echo ""
    echo "Usually, installation should be run first and checked if everything was installed"
    echo "successfully.  Then, for the bootstrapping, a g2p model is required."
}

isInSet () {
    local e
    for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
    return 1
}

while [[ $# > 0 ]]; do
    key="$1"
    case $key in
        -h|--help)
            printHelp
            exit 0
            ;;
        -i|--install)
            echo "Will install and exit"
            install
            echo "Installation complete"
            exit 0
            ;;
        -l|--language)
            language="$2"
            if ! isInSet "$language" "english" "german"; then
                echo >&2 "Error: Only 'english' and 'german' are supported languages"
                exit 1
            fi
            shift 2
            ;;
        -f|--ffmpeg)
            ffmpeg="$2"
            shift 2
            ;;
        -g|--g2p)
            G2P_model="$2"
            shift 2
            ;;
        -c|--caps)
            G2P_model_CAPS=true
            shift
            ;;
        -m|--model)
            model="$2"
            shift 2
            ;;
        -s|--ser)
            ser="$2"
            shift 2
            ;;
        -d|--dic)
            dic="$2"
            shift 2
            ;;
        *)
            ;;
    esac
done

if [ "$G2P_model" = "" ]; then
    echo "a G2P model is required"
    exit 1
fi

bootstrap 3
