import difflib
import re

in_tokens = ["Der ", "Berg ", "Mt.", "Everest ", "ist ", "8000 ", "m ", "hoch", "."]
norm_tokens = ["Der", "Berg", "Mount", "Everest", "ist", "acht tausend", "Meter", "hoch", "."]


def extract_norm_word(word):
    return re.sub(r'[^\w]', '', word)


def test(a, b):
    a = list(map(extract_norm_word, a))
    b = list(map(extract_norm_word, b))
    differ = difflib.SequenceMatcher(None, a, b, False)
    codes = differ.get_opcodes()
    return codes
