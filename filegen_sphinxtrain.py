#! /usr/bin/env python3

# This script can be used to generate various files needed for
# sphinxtrain. Depending on which files are given as input, all
# outputfiles which are possible to generate will be generated.
# Possible modi of operation:
#
# snippetdir -> fileids, transcripts
#     given a directory with the structure like this:
#
#     wav/
#       sample_dir1/
#         sample01.wav
#         sample01.txt
#         sample02.wav
#         sample02.txt
#         ...
#       sample_dir2/
#         ...
#
#     the text files which have a corresponding wav file will be taken
#     as transcript files and their contents will be put into the
#     transcripts file.  the matching filenames will be written into
#     the fileids file.
#
# transcripts -> words
#     given a transcripts file, all the unique words will be taken and
#     written into the words file.
#
# g2p_bin, model, words -> dictionary, errors
#     given the path to an executable file which takes a model file as
#     first argument and a words file as a second argument, the
#     executable will be called with the given filenames and its
#     stdout will be piped to the dictionary file; stderr to the
#     errors file.
#     This is must useful with the apply_model.sh file generated by
#     the install_sequitur.sh script.
#
# errors, transcripts, fileids -> transcripts, fileids
#     given the error file generated in the previous step, the words
#     which couldn't be transcripted are a parsed and the transcript
#     lines which contain those words are removed, together with the
#     corresponding lines from the fileids file.
#
# dictionary -> phones
#     given a dictionary file, all the used phones are parsed and
#     written into the phones file, one per line.
#
# -> fillers
#     this functionality takes no input, it just generates the default
#     fillers file.
#
#
# example call:
#     filegen_sphinxtrain.py -s wav/
#                            -f db_train.fileids -t db_train.transcripts
#                            -w db.words -g sequitur/apply_model.sh
#                            -m vm_ger-noacc.model.9
#                            -e db.errors -d db.dict -p db.phones
#                            --fillers db.fillers
#
# the script currently does not handle the files required by
# shphinxtrain for testing (db_test.fileids, db_test.transcripts, the
# language model).

import argparse
import logging
import os
import os.path
import random
import re
from shutil import copyfile
import subprocess
import sys

parser = argparse.ArgumentParser(description="""A script to generate the
*.fileids and *.transcription files from a directory containing
snippets""")

parser.add_argument("-v", "--verbose",
                    help="increase verbosity",
                    action="count", default=0)


subparsers = parser.add_subparsers(help="sub command to execute",
                                   dest="sub_command_name")

parser_cfg = subparsers.add_parser("cfg", help="modify the config file")

parser_cfg.add_argument("cfg_file",
                        nargs="?", default="etc/sphinx_train.cfg",
                        help="the config file usually located in etc/sphinx_train.cfg")

parser_gen = subparsers.add_parser("gen", help="generate all the files")

parser_gen.add_argument("-p", "--prefix",
                        help="directory prefix to task directory")
parser_gen.add_argument("-T", "--no_test",
                        help="prevents splitting of data into test and train",
                        action="store_true")
parser_gen.add_argument("-c", "--caps",
                        help="convert all text to ALL-CAPS",
                        action="store_true")
parser_gen.add_argument("g2p_bin",
                        help="the path to the binary for generating a g2p model")
parser_gen.add_argument("g2p_model",
                        help="the model required by sequitur")
parser_gen.add_argument("name",
                        help="the name of the task as it is used in sphinxtrain")
parser_gen.add_argument("wav_dir",
                        nargs="?", default="wav",
                        help="the path to the wav directory containing the samples")
parser_gen.add_argument("split",
                        nargs="?", type=float, default=0.1,
                        help="the portion used for testing, default is 0.1")

FILENAMES = {"snippetsdir":         "wav",
             "fileids":             "etc/misc.{name}.fileids",
             "test_fileids":        "etc/{name}_test.fileids",
             "train_fileids":       "etc/{name}_train.fileids",
             "transcription":       "etc/misc.{name}.transcription",
             "test_transcription":  "etc/{name}_test.transcription",
             "train_transcription": "etc/{name}_train.transcription",
             "words":               "etc/misc.{name}.words",
             "errors":              "etc/misc.{name}.g2p_errors",
             "dictionary":          "etc/{name}.dic",
             "phones":              "etc/{name}.phone",
             "fillers":             "etc/{name}.filler"}
# {name}.lm.DMP is missing for the language model


## setup logger
logger = logging.getLogger()
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter(fmt="%(levelname)s: %(message)s"))
ch.setLevel(logging.DEBUG) # handle everything
logger.addHandler(ch)

FILLERS="""
<s> SIL
</s> SIL
<sil> SIL
""".strip()


### generative functions

def gen_files(snippet_dir, f_filename, t_filename, caps=False, verbose=False):
    try:
        fileids = open(f_filename, "w")
        transcripts = open(t_filename, "w")
    except Exception as e:
        sys.stderr.write("Error openening files to write to:\n" + str(e) + "\n")
        sys.stderr.flush()
        exit(1)
    articles = os.listdir(snippet_dir)
    for a in articles:
        path = os.path.join(snippet_dir, a)
        if os.path.isdir(path):
            if verbose:
                sys.stderr.write("Handling article: {}\n".format(a))
                sys.stderr.flush()
            files = os.listdir(path)
            filenames = sorted(list(set([os.path.splitext(f)[0] for f in files])))
            for f in filenames:
                if f + ".txt" in files and f + ".wav" in files:
                    if verbose:
                        sys.stderr.write(f + "\n")
                        sys.stderr.flush()
                    fileids.write(os.path.join(a, f) + "\n")
                    with open(os.path.join(path, f + ".txt")) as transcript_file:
                        transcript = transcript_file.read().strip()
                        if caps:
                            transcript = transcript_to_upper(transcript)
                        transcript += " ({})".format(f)
                    transcripts.write(transcript + "\n")
    fileids.close()
    transcripts.close()


def transcript_to_upper(text):
    """converts all words to uppercase, but not the start and stop marks"""
    text = text.upper()
    text = re.sub("<S>", "<s>", text) # kinda hacky but it works I guess
    text = re.sub("</S>", "</s>", text)
    return text


def get_transcript_text(line):
    return re.match(r"^<s> (.*) </s> \(.*\)$", line.strip()).groups()[0]


def gen_wordlist(t_filename, w_filename, verbose=False):
    words = set()
    with open(t_filename) as f:
        lines = f.read().splitlines()
    if verbose:
        sys.stderr.write("Transcript lines: {}\n".format(len(lines)))
        sys.stderr.flush()
    for line in lines:
        text = get_transcript_text(line)
        ws = text.split()
        words = words.union(ws)
    if verbose:
        sys.stderr.write("Unique words: {}\n".format(len(words)))
        sys.stderr.flush()
    words = sorted(words)
    with open(w_filename, "w") as f:
        for w in words:
            f.write(w + "\n")


def call_sequitur(g2p_path, model_path, words_path, dict_path, errors_path):
    with open(errors_path, "w") as errors, open(dict_path, "w") as dict:
        process = subprocess.Popen([g2p_path, model_path, words_path],
                                   stdout=dict, stderr=errors)
        process.communicate()


def parse_untranslatable_strings(errors_path):
    strs = []
    with open(errors_path) as errors:
        lines = errors.read().splitlines()
    for line in lines:
        if line.startswith("failed to convert"):
            string = re.match(r'^failed to convert "(.*)": translation failed$', line).groups()[0]
            strs.append(string)
    return strs


def remove_lines(file_path, line_numbers):
    with open(file_path) as f:
        lines = f.readlines()
    with open(file_path, "w") as f:
        for i, line in enumerate(lines):
            if i in line_numbers:
                continue
            else:
                f.write(line)
    
        
def remove_erroneous_entries(errors_path, fileids_path, transcript_path):
    """reads untranslatable strings from the error file, checks the transcript lines
    for these strings and removes the offending lines in the transcript and fileid files"""
    untransl_strs = parse_untranslatable_strings(errors_path)
    logger.info("found {} untranslatable strings".format(len(untransl_strs)))
    indices_to_remove = []
    with open(transcript_path) as transcripts:
        lines = transcripts.read().splitlines()
    texts = [get_transcript_text(l) for l in lines]
    for i, text in enumerate(texts):
        for s in untransl_strs:
            if s in text:
                indices_to_remove.append(i)
                break
    logger.info("found {} lines to remove".format(len(indices_to_remove)))
    remove_lines(fileids_path, indices_to_remove)
    remove_lines(transcript_path, indices_to_remove)


def generate_phones(dictionary_path, phones_path):
    phones = set()
    phones.add("SIL") # silence
    with open(dictionary_path) as dictionary:
        lines = dictionary.readlines()
    for l in lines:
        ps = l.split()[1:]
        phones = phones.union(ps)
    phones = sorted(phones)
    with open(phones_path, "w") as p:
        for phone in phones:
            p.write(phone + "\n")


def split_test_train(fileids_path, transcription_path, test_share, test_fids_path, test_ts_path, train_fids_path, train_ts_path):
    with open(fileids_path) as f:
        fileids = f.read().splitlines()
    with open(transcription_path) as f:
        transcriptions = f.read().splitlines()
    pairs = list(zip(fileids, transcriptions))
    random.shuffle(pairs)
    for_testing = int(test_share * len(pairs))
    testing = pairs[:for_testing]
    training = pairs[for_testing:]
    with open(test_fids_path, "w") as f1, open(test_ts_path, "w") as f2:
        for pair in testing:
            f1.write(pair[0] + "\n")
            f2.write(pair[1] + "\n")
    with open(train_fids_path, "w") as f1, open(train_ts_path, "w") as f2:
        for pair in training:
            f1.write(pair[0] + "\n")
            f2.write(pair[1] + "\n")


def no_test(fileids_path, transcription_path, train_fids_path, train_ts_path):
    copyfile(fileids_path, train_fids_path)
    copyfile(transcription_path, train_ts_path)


def generate_fillers(fillers_path):
    with open(fillers_path, "w") as f:
        f.write(FILLERS)


def modify_cfg_file(cfg_file_path):
    with open(cfg_file_path) as f:
        cfg = f.read()
    cfg = re.sub("CFG_FINAL_NUM_DENSITIES = 8", "CFG_FINAL_NUM_DENSITIES = 16", cfg)
    cfg = re.sub("CFG_N_TIED_STATES = 200", "CFG_N_TIED_STATES = 4000", cfg)
    cfg = re.sub("CFG_NPART = 1", "CFG_NPART = 24", cfg)
    cfg = re.sub('CFG_FORCE_ALIGN_MODELDIR = "$CFG_MODEL_DIR/$CFG_EXPTNAME.falign_ci_$CFG_DIRLABEL";',
                 'CFG_FORCE_ALIGN_MODELDIR = "$CFG_BASE_DIR/forcealign_model";', cfg)
    cfg = re.sub("CFG_LDA_MLLT = 'no'", "CFG_LDA_MLLT = 'yes'", cfg)
    cfg = re.sub("CFG_LDA_DIMENSION = 29", "CFG_LDA_DIMENSION = 32", cfg)
    cfg = re.sub("CFG_CONVERGENCE_RATIO = 0.1", "CFG_CONVERGENCE_RATIO = 0.04", cfg)
    cfg = re.sub('CFG_QUEUE_TYPE = "Queue"', 'CFG_QUEUE_TYPE = "Queue::POSIX"', cfg)
    with open(cfg_file_path, "w") as f:
        f.write(cfg)
    

### main function and preparing arguments

def set_log_level(verbosity):
    if verbosity == 0:
        logger.setLevel(logging.WARNING)
    elif verbosity == 1:
        logger.setLevel(logging.INFO)
    elif verbosity >= 2:
        logger.setLevel(logging.DEBUG)


def create_parent_dirs(filenames):
    dirs = []
    for fn in filenames:
        dir = os.path.dirname(fn)
        if dir != "":
            dirs.append(dir)
    for dir in dirs:
        os.makedirs(dir, exist_ok=True)


def main_everything(args):
    fns = FILENAMES.copy()
    for k in fns:
        fns[k] = os.path.join(args.prefix, fns[k])
        fns[k] = fns[k].format(name=args.name)
    create_parent_dirs(fns.values())
    logger.info("using snippets dir to generate fileids and transcriptions")
    gen_files(fns["snippetsdir"], fns["fileids"], fns["transcription"], caps=args.caps, verbose=args.verbose)
    logger.info("using transcriptions file to generate wordlist")
    gen_wordlist(fns["transcription"], fns["words"], verbose=args.verbose)
    logger.info("calling sequitur to generate dictionary")
    call_sequitur(args.g2p_bin, args.g2p_model, fns["words"], fns["dictionary"], fns["errors"])
    logger.info("using sequitur errors to remove non-translatable lines from transcriptions")
    remove_erroneous_entries(fns["errors"], fns["fileids"], fns["transcription"])
    logger.info("generating the phonelist from the dictionary")
    generate_phones(fns["dictionary"], fns["phones"])
    logger.info("generating default fillers file")
    generate_fillers(fns["fillers"])
    if args.no_test:
        no_test(fns["fileids"], fns["transcription"],
                fns["train_fileids"], fns["train_transcription"])
    else:
        logger.info("splitting fileids and transcription into testing and training")
        split_test_train(fns["fileids"], fns["transcription"], args.split,
                         fns["test_fileids"], fns["test_transcription"],
                         fns["train_fileids"], fns["train_transcription"])
        
        
def main():
    args = parser.parse_args()
    set_log_level(args.verbose)
    if args.sub_command_name == "gen":
        main_everything(args)
    elif args.sub_command_name == "cfg":
        modify_cfg_file(args.cfg_file) # TODO possibly some logging
    else:
        pass # we should never get here


if __name__ == "__main__":
    main()
