#!/usr/bin/env python3

import argparse
import json
import os
import os.path
import sys
import code


parser = argparse.ArgumentParser(description="Statistics for Spoken Wikipedia Data")

parser.add_argument("-a", "--articles",
                    help="the directory where article data and audio is stored",
                    default="./articles")
parser.add_argument("-r", "--results",
                    help="the directory where the generated alignment data is stored",
                    default="./results")
parser.add_argument("-p", "--path-format",
                    help="the way article names should be printed. Full path or directory name only",
                    choices=["full", "dir"], default="dir")

subparsers = parser.add_subparsers(help="sub command to execute",
                                   dest="sub_command_name")
parser_articles = subparsers.add_parser("articles", help="numbers to outline quality of the downloaded articles")
group = parser_articles.add_mutually_exclusive_group()
group.add_argument("--list-missing-audio",
                   help="print the articles that are missing audio",
                   action="store_true")
group.add_argument("--list-missing-text",
                   help="print the articles that are missing text",
                   action="store_true")
group.add_argument("--list-missing-both",
                   help="print the articles that are missing both text and audio",
                   action="store_true")

parser_results = subparsers.add_parser("results", help="basics statistics to summarize results")
parser_results.add_argument("--list-missing-alignment",
                            help="print the directories that are missing the align.data.json file",
                            action="store_true")
parser_results.add_argument("--list-errors",
                            help="print the directories that are missing the align.data.json file and tries to find out what the problem is",
                            action="store_true")
parser_results.add_argument("-c", "--column",
                            help="which column to use for sorting (0-based index)",
                            type=int, choices=range(3), default=1)
parser_results.add_argument("-r", "--reverse",
                            help="reverse order when sorting",
                            action="store_true")


def result_fmt(dir, args):
    if args.path_format == "full":
        return os.path.join(args.results, dir)
    else:
        return dir


def articles_summary(args):
    sys.stdout.write("articles summary\n")
    articles_dir = args.articles
    articles = os.listdir(articles_dir)
    missing_audio = []
    missing_text = []
    for a in articles:
        path = os.path.join(articles_dir, a)
        files = os.listdir(path)
        if not "audio.wav" in files:
            missing_audio.append(path)
        if not "audio.txt" in files:
            missing_text.append(path)
    missing_both = [a for a in missing_audio if a in missing_text]
    sys.stdout.write("articles:       {:>4}\n".format(len(articles)))
    sys.stdout.write("missing audio:  {:>4}\n".format(len(missing_audio)))
    sys.stdout.write("missing text:   {:>4}\n".format(len(missing_text)))
    sys.stdout.write("missing both:   {:>4}\n".format(len(missing_both)))
    sys.stdout.flush()
    if args.list_missing_audio:
        for p in missing_audio:
            sys.stdout.write(p + "\n")
    if args.list_missing_text:
        for p in missing_text:
            sys.stdout.write(p + "\n")
    if args.list_missing_both:
        for p in missing_both:
            sys.stdout.write(p + "\n")


class ResultsSummary():

    def __init__(self, args):
        self.args = args

    def run(self):
        if self.args.list_missing_alignment:
            self.get_missing_alignment(print=True)
        else:
            self.results_summary()

    def get_missing_alignment(self, print=False):
        alignment_available = []
        missing_alignment = []
        for r in os.listdir(self.args.results):
            print_name = result_fmt(r, self.args)
            path = os.path.join(self.args.results, r)
            if os.path.isfile(os.path.join(path, "steps", "align.data.json")):
                alignment_available.append(path)
            else:
                missing_alignment.append(path)
                if self.args.list_missing_alignment:
                    sys.stdout.write(print_name)
                    sys.stdout.flush()
        return alignment_available, missing_alignment

    def results_summary(self):
        sys.stdout.write("results summary\n")
        results = os.listdir(self.args.results)
        alignment_available, missing_alignment = self.get_missing_alignment()
        sys.stdout.write("generated directories: {:>4}\n".format(len(results)))
        sys.stdout.write("missing alignment:     {:>4}\n".format(len(missing_alignment)))
        sys.stdout.flush()
        t_words, t_words_aligned, t_sentences, t_sentences_aligned = 0, 0, 0, 0
        rows = []
        for p in alignment_available:
            with open(os.path.join(p, "steps", "align.data.json")) as f:
                a_data = json.loads(f.read())
            ws_aligned, ws = words_aligned(a_data)
            t_words += ws
            t_words_aligned += ws_aligned
            ss_aligned, ss = sentences_aligned(a_data)
            t_sentences += ss
            t_sentences_aligned += ss_aligned
            rows.append((p, ws_aligned / ws, ss_aligned / ss, ws))
        max_art_width = max([len(r[0]) for r in rows])
        row_fmt = "{:<" + str(max_art_width) + "}  {:>7.2%}  {:>7.2%}  {:>9,}"
        rows = sorted(rows, key = lambda r: r[self.args.column], reverse = self.args.reverse)
        sys.stdout.write("article, % words aligned, % sentences aligned, total words\n")
        for r in rows:
            sys.stdout.write(row_fmt.format(*r) + "\n")
        sys.stdout.write("\n")
        sys.stdout.write("total words aligned:     {:>7.2%}\n".format(t_words_aligned / t_words))
        sys.stdout.write("total sentences aligned: {:>7.2%}\n".format(t_sentences_aligned / t_sentences))
        sys.stdout.flush()
        

def print_tabular_data(rows, alignment, type=None): # not in use
    widths = [max([len(str(r[c])) for r in rows]) for c in range(len(rows[0]))]
    row_fmt = "".join(["{:" + alignment[i] + str(widths[i]) + (type[i] if type  else "") + "}  "
                       for i in range(len(rows[0]))])
    for r in rows:
        sys.stdout.write(row_fmt.format(*r) + "\n")
    sys.stdout.flush()

### analyzing alignment data

def word_is_mapped(w):
    return 'start' in w and 'stop' in w

def word_is_silent(w):
    return w['normalized'] == ""

def word_is_aligned(w):
    return word_is_silent(w) or word_is_mapped(w)


def words_aligned(a_data):
    non_silent_words = 0
    aligned_words = 0
    for w in a_data["words"]:
        if not word_is_silent(w):
            non_silent_words += 1
            if word_is_mapped(w):
                aligned_words += 1
    return (aligned_words, non_silent_words)


def sentences_aligned(a_data):
    sentences = len(a_data["sentences"])
    complete_sentences = 0
    for s in a_data["sentences"]:
        words = a_data["words"][s["start_word"]:s["end_word"]]
        if all([word_is_aligned(w) for w in words]):
            complete_sentences += 1
    return (complete_sentences, sentences)
        
    


def main():
    args = parser.parse_args()
#    code.interact(local=locals())

    if args.sub_command_name == "articles":
        articles_summary(args)
    elif args.sub_command_name == "results":
        s = ResultsSummary(args)
        s.run()
    else:
        print("No subcommand given. Use --help for help.")
    


if __name__ == "__main__":
    main()
